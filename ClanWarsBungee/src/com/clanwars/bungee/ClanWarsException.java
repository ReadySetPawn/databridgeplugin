package com.clanwars.bungee;

public class ClanWarsException extends RuntimeException {

    public ClanWarsException(String msg){
        super(msg);
    }

}
