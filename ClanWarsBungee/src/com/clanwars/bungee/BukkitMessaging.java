package com.clanwars.bungee;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.*;
import java.util.UUID;

public class BukkitMessaging implements Listener {

    private Main main;

    public BukkitMessaging(Main main){
        this.main = main;
    }

    @EventHandler
    public void onListen(PluginMessageEvent e){
        if (e.getTag().equalsIgnoreCase("BungeeCord")){
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(e.getData()));
            try {
                if (in.readUTF().equalsIgnoreCase("playerdata")){
                    UUID uuid = UUID.fromString(in.readUTF());
                    ClanPlayer clanPlayer = ClanPlayer.getPlayer(uuid);
                    //sendPlayerData(clanPlayer);
                }
            } catch (IOException ex){
                ex.printStackTrace();
            }
        }
    }

    public void sendPlayerData(ClanPlayer clanPlayer){
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        DataOutputStream dout = new DataOutputStream(bout);

        try {
            dout.writeUTF("playerdata");
            dout.writeUTF(clanPlayer.getUuid().toString());
            dout.writeUTF(clanPlayer.getName());
            dout.writeUTF(clanPlayer.getClan());
            dout.writeInt(clanPlayer.getXp());
            dout.writeInt(clanPlayer.getLevel());
            dout.writeInt(clanPlayer.getClanRank().getId());
            dout.writeLong(clanPlayer.getRawWeaponData());
        } catch (IOException e){
            e.printStackTrace();
        }
        clanPlayer.getBungeePlayer().getServer().getInfo().sendData("Return", bout.toByteArray());
    }

    private void send(String channel, String msg, ServerInfo server){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

        try {
            dataOutputStream.writeUTF(channel);
            dataOutputStream.writeUTF(msg);
        } catch (IOException e){
            e.printStackTrace();
        }
        server.sendData("Return", byteArrayOutputStream.toByteArray());
    }

}
