package com.clanwars.bungee;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.*;

public class ClanPlayer {

    static final HashMap<UUID, ClanPlayer> PLAYERS = new HashMap<>();

    private final Main main;
    private final UUID uuid;
    private final ProxiedPlayer bungeePlayer;

    private String name;

    private String clan;

    private int xp;

    private int level;

    private ClanRank clanRank;

    private List<Weaponry> weapons;

    private long rawWeaponData;

    public ClanPlayer(Main main, ProxiedPlayer bungeePlayer){
        this(main, bungeePlayer.getUniqueId());
    }

    public ClanPlayer(Main main, UUID uuid){
        this.main = main;
        this.uuid = uuid;
        if (PLAYERS.containsKey(uuid)){
            throw new ClanWarsException(String.format("ClanPlayer with uuid '%s' already exists", uuid.toString()));
        }
        PLAYERS.put(uuid, this);
        this.bungeePlayer = BungeeCord.getInstance().getPlayer(uuid);
    }

    public void sendMessage(String msg){
        bungeePlayer.sendMessage(msg);
    }

    public void throwAway(){
        PLAYERS.remove(uuid);
    }

    public void loadPlayerData(){
        if (name != null){
            throw new ClanWarsException("Player data can only be initialized once");
        }
        main.getClanWarsMysql().loadPlayerData(this);
    }

    public void setData(Object[] data){
        if (name != null){
            throw new ClanWarsException("Player data can only be initialized once");
        }
        this.name = (String) data[0];
        this.clan = (String) data[1];
        this.xp = (Integer) data[2];
        this.level = (Integer) data[3];
        this.clanRank = ClanRank.getByID((Integer) data[4]);
        loadWeaponsData((Long) data[5]);
        main.getBukkitMessaging().sendPlayerData(this);
    }

    public void loadWeaponsData(long data){
        if (weapons != null){
            throw new ClanWarsException("Weapon data can only be loaded once per ClanPlayer instance. Use updateWeapons(...) instead.");
        }
        this.rawWeaponData = data;
        this.weapons = new ArrayList<>();
        for (int i = 0; i < 63; i++){
            if ((data & 1) == 1){
                weapons.add(Weaponry.getByID(i));
            }
            data = data >> 1;
        }
    }

    public void updateWeapon(int id, boolean unlocked){
        if (unlocked){
            weapons.add(Weaponry.getByID(id));
        } else {
            weapons.remove(Weaponry.getByID(id));
        }
    }

    public String getName(){
        return bungeePlayer.getName();
    }

    public ProxiedPlayer getBungeePlayer() {
        return bungeePlayer;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getClan() {
        return clan;
    }

    public ClanRank getClanRank() {
        return clanRank;
    }

    public int getLevel() {
        return level;
    }

    public int getXp() {
        return xp;
    }

    public long getRawWeaponData() {
        return rawWeaponData;
    }

    public static ClanPlayer getPlayer(UUID uuid){
        return PLAYERS.get(uuid);
    }
}
