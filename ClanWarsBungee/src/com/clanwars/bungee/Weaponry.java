package com.clanwars.bungee;

public enum Weaponry {

    SWORD(0);

    private int id;

    Weaponry(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Weaponry getByID(int id){
        for (Weaponry weapon : values()){
            if (weapon.getId() == id){
                return weapon;
            }
        }
        return null;
    }

}
