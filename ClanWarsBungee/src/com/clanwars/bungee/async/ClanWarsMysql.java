package com.clanwars.bungee.async;

import com.clanwars.bungee.ClanPlayer;
import com.clanwars.bungee.Main;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import java.sql.*;
import java.util.UUID;

public class ClanWarsMysql {

    private Main main;
    private MysqlExecutor mysqlExecutor;
    private Connection connection;

    public ClanWarsMysql(Main main){
        this.main = main;
        this.mysqlExecutor = new MysqlExecutor(new MysqlDataSource());

        try {
            this.connection = mysqlExecutor.getMysqlDataSource().getConnection();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void checkAndRegisterPlayer(ClanPlayer clanPlayer){
        UUID uuid = clanPlayer.getUuid();

        /*
         * Insert into players table if row does not exist already.
         * The players table stores general information about the player
         * such as name, clan, rank, xp, level, etc.
         */

        String checkQuery = String.format("SELECT * FROM players where uuid='%s';", uuid.toString());
        new MysqlQuery(checkQuery, false){
            @Override
            public void onCompletion(ResultSet resultSet) throws SQLException {
                if (!resultSet.next()){
                    String query = String.format("INSERT INTO players VALUES ('%s', '%s', '%s', %d, %d, %d);", uuid.toString(), clanPlayer.getName(), "TestClan", 0, 0, 0);
                    new MysqlQuery(query, true){
                        @Override
                        public void onCompletion(ResultSet resultSet) throws SQLException {
                            clanPlayer.sendMessage("You have been added");
                        }
                    };
                } else {
                    clanPlayer.sendMessage("Welcome back!");
                }
            }
        };

        /*
         * Insert into playerunlocks table if row does not exist already.
         * The playerunlocks table stores all information about what items the
         * player has available. Each item is assigned a boolean. And each
         * item is grouped by its type (i.e. weapons, particles, rides, builds, etc.)
         * Currently, each category is assigned one column with type BIGINT that stores
         * 63 (minus the sign bit) booleans.
         */

        String unlocksCheckQuery = String.format("SELECT * FROM playerunlocks where uuid='%s';", uuid.toString());
        new MysqlQuery(unlocksCheckQuery, false){
            @Override
            public void onCompletion(ResultSet resultSet) throws SQLException {
                if (!resultSet.next()){
                    String query = String.format("INSERT INTO playerunlocks VALUES ('%s', '%d', '%d');", uuid.toString(), 0, 0);
                    new MysqlQuery(query, true){
                        @Override
                        public void onCompletion(ResultSet resultSet) throws SQLException {
                            clanPlayer.sendMessage("You have been added to playerunlocks table");
                            clanPlayer.loadPlayerData();
                        }
                    };
                } else {
                    clanPlayer.loadPlayerData();
                }
            }
        };
    }

    public void loadPlayerData(ClanPlayer clanPlayer){
        Object[] data = new Object[6];

        String generalQuery = String.format("SELECT * FROM players WHERE uuid='%s';", clanPlayer.getUuid());
        new MysqlQuery(generalQuery, false){
            public void onCompletion(ResultSet resultSet) throws SQLException {
                if (resultSet.next()) {
                    data[0] = resultSet.getString(2);
                    data[1] = resultSet.getString(3);
                    data[2] = resultSet.getInt(4);
                    data[3] = resultSet.getInt(5);
                    data[4] = resultSet.getInt(6);
                }
            }
        };

        String unlocksQuery = String.format("SELECT * FROM playerunlocks WHERE uuid='%s';", clanPlayer.getUuid());
        new MysqlQuery(unlocksQuery, false){
            @Override
            public void onCompletion(ResultSet resultSet) throws SQLException {
                if (resultSet.next()) {
                    data[5] = resultSet.getLong(1);
                    clanPlayer.setData(data);
                }
            }
        };
    }


}
