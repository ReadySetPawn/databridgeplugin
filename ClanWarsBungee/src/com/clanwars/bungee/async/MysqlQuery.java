package com.clanwars.bungee.async;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class MysqlQuery {

    private String query;
    private boolean update;

    public MysqlQuery(String query, boolean update){
        this.query = query;
        this.update = update;
        MysqlExecutor.addQuery(this);
    }

    public String getQuery() {
        return query;
    }

    public boolean isUpdate() {
        return update;
    }

    public abstract void onCompletion(ResultSet resultSet) throws SQLException;

}
