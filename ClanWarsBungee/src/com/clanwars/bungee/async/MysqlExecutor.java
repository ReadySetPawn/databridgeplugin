package com.clanwars.bungee.async;


import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Queue;

public class MysqlExecutor implements Runnable {

    static Thread thread;
    static volatile Queue<MysqlQuery> queue = new LinkedList<>();

    private MysqlDataSource mysqlDataSource;

    public MysqlExecutor(MysqlDataSource mysqlDataSource){
        thread = new Thread(this, "MysqlThread");
        this.mysqlDataSource = mysqlDataSource;

        mysqlDataSource.setUser("app");
        mysqlDataSource.setPassword("20420340");
        mysqlDataSource.setServerName("107.170.201.115");
        mysqlDataSource.setDatabaseName("clanwars");

        thread.start();
    }

    @Override
    public void run() {
        while (true){
            if (!queue.isEmpty()){
                MysqlQuery mysqlQuery = queue.remove();
                ResultSet resultSet = null;
                try {
                    PreparedStatement statement = mysqlDataSource.getConnection().prepareStatement(mysqlQuery.getQuery());
                    if (mysqlQuery.isUpdate()){
                        statement.executeUpdate();
                    } else {
                        resultSet = statement.executeQuery();
                    }
                    mysqlQuery.onCompletion(resultSet);
                } catch (SQLException e){
                    e.printStackTrace();
                }
            } else {
                try {
                    synchronized (thread){
                        thread.wait();
                    }
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
    }

    public static void addQuery(MysqlQuery mysqlQuery){
        queue.add(mysqlQuery);
        synchronized (thread){
            thread.notify();
        }
    }

    public MysqlDataSource getMysqlDataSource() {
        return mysqlDataSource;
    }
}
