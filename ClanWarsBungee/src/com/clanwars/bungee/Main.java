package com.clanwars.bungee;

import com.clanwars.bungee.async.ClanWarsMysql;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;



public class Main extends Plugin implements Listener {

    private ClanWarsMysql clanWarsMysql;
    private BukkitMessaging bukkitMessaging;

    public void onEnable(){
        this.clanWarsMysql = new ClanWarsMysql(this);
        this.bukkitMessaging = new BukkitMessaging(this);

        BungeeCord.getInstance().registerChannel("Return");

        getProxy().getPluginManager().registerListener(this, this);
        getProxy().getPluginManager().registerListener(this, bukkitMessaging);
    }

    @EventHandler
    public void onJoin(PostLoginEvent e){
        ProxiedPlayer player = e.getPlayer();

        if (!ClanPlayer.PLAYERS.containsKey(player.getUniqueId())){
            ClanPlayer clanPlayer = new ClanPlayer(this, player);
            clanWarsMysql.checkAndRegisterPlayer(clanPlayer);
        }

    }

    @EventHandler
    public void onQuit(PlayerDisconnectEvent e){
        ProxiedPlayer player = e.getPlayer();
        ClanPlayer clanPlayer = ClanPlayer.getPlayer(player.getUniqueId());
        clanPlayer.throwAway();
    }

    public ClanWarsMysql getClanWarsMysql() {
        return clanWarsMysql;
    }

    public BukkitMessaging getBukkitMessaging() {
        return bukkitMessaging;
    }
}
