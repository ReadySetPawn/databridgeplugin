package com.clanwars.bungee;

public enum ClanRank {

    BEGINNER(0);


    private int id;

    ClanRank(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static ClanRank getByID(int id){
        for (ClanRank clanRank : values()){
            if (clanRank.getId() == id){
                return clanRank;
            }
        }
        return null;
    }

}
